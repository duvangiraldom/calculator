angular.module('apiCalculator', [])
.controller('ExampleController', function($scope, $http) {

     $scope.sum = function() {
	    $http.get('http://localhost:8081/calculator/sum/' +  $scope.num1 +'/'+  $scope.num2).
		then(function(response) {
		    $scope.greeting = response.data;
		});
    }

    $scope.rest = function() {
	    $http.get('http://localhost:8081/calculator/rest/' +  $scope.num1 +'/'+  $scope.num2).
		then(function(response) {
		    $scope.greeting = response.data;
		});
    }

    $scope.multiplication = function() {
	    $http.get('http://localhost:8081/calculator/multiplication/' +  $scope.num1 +'/'+  $scope.num2).
		then(function(response) {
		    $scope.greeting = response.data;
		});
    }

    $scope.division = function() {
	    $http.get('http://localhost:8081/calculator/division/' +  $scope.num1 +'/'+  $scope.num2).
		then(function(response) {
		    $scope.greeting = response.data;
		});
    }
});
