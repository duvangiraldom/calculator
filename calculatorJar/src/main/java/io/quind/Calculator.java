package io.quind;

public class Calculator {

	public int sumBetweenTwoNumbers(int num1, int num2) {
		return num1 + num2;
	}

	public int restBetweenTwoNumbers(int num1, int num2) {
		return num1 - num2;
	}

	public int multiplicationBetweenTwoNumbers(int num1, int num2) {
		return num1 * num2;
	}

	public float divisionBetweenTwoNumbers(int num1, int num2) throws Exception {
		if (num2 != 0) {
			return num1 / num2;
		}
		else {
			throw new Exception("Error to divide number by zero");
		}
	}

}
