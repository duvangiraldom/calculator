package io.quind;

import org.junit.Assert;
import org.junit.Test;

import io.quind.Calculator;

public class CalculatorTest {

	private Calculator calculator = new Calculator();
	
	@Test
	public void testingSumBetweenTwoNumbers() {
		int num1 = 5;
		int num2 = 20;
		int result = 25;

		int sumBetweenTwoNumbers = calculator.sumBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, sumBetweenTwoNumbers);
	}
	
	@Test
	public void testingRestBetweenTwoNumbers() {
		int num1 = 14;
		int num2 = 10;
		int result = 4;
		
		int restBetweenTwoNumbers = calculator.restBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, restBetweenTwoNumbers);
	}
	
	@Test
	public void testingMultiplicationMBetweenTwoNumbers() {
		int num1 = 4;
		int num2 = 6;
		int result = 24;
		
		int multiplicationBetweenTwoNumbers = calculator.multiplicationBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, multiplicationBetweenTwoNumbers);
	}
	
	@Test
	public void testingDivisionBetweenTwoNumbers() throws Exception {
		int num1 = 9;
		int num2 = 3;
		float despise = 0.001f;
		float result = 3f;

		try {
			float divisionBetweenTwoNumbers = calculator.divisionBetweenTwoNumbers(num1, num2);
			Assert.assertEquals(result, divisionBetweenTwoNumbers, despise);
		}
		catch (Exception errorToDivide) {
			System.out.println(errorToDivide.getMessage());
		}
	}
	
	@Test(expected = Exception.class)
	public void testingDivisionByZero() throws Exception {
		int num1 = 9;
		int num2 = 0;
		
		calculator.divisionBetweenTwoNumbers(num1, num2);
	}
	
}
