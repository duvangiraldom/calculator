package io.quind.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.quind.Calculator;
import io.quind.general.CalculatorJson;

@RestController
@RequestMapping("/calculator")
public class calculatorController {
	
	private Calculator calculator = new Calculator(); 
	
	@RequestMapping(value = "/sum/{num1}/{num2}", method = RequestMethod.GET)
	public CalculatorJson sumBetweenTwoNumbers(@PathVariable("num1") int num1, @PathVariable("num2") int num2) {	
		int result = calculator.sumBetweenTwoNumbers(num1, num2);
		return new CalculatorJson(num1, num2, result);
	}

	@RequestMapping(value = "/rest/{num1}/{num2}", method = RequestMethod.GET)
	public CalculatorJson restBetweenTwoNumbers(@PathVariable("num1") int num1, @PathVariable("num2") int num2) {
		int result = calculator.restBetweenTwoNumbers(num1, num2);
		return new CalculatorJson(num1, num2, result);
	}
	
	@RequestMapping(value = "/multiplication/{num1}/{num2}", method = RequestMethod.GET)
	public CalculatorJson multiplicationBetweenTwoNumbers(@PathVariable("num1") int num1, @PathVariable("num2") int num2) {
		
		int result = calculator.multiplicationBetweenTwoNumbers(num1, num2);
		return new CalculatorJson(num1, num2, result);
	}
	
	@RequestMapping(value = "/division/{num1}/{num2}", method = RequestMethod.GET)
	public CalculatorJson divisionBetweenTwoNumbers(@PathVariable("num1") int num1, @PathVariable("num2") int num2)  throws Exception {
		float result = calculator.divisionBetweenTwoNumbers(num1, num2);
		return new CalculatorJson(num1, num2, (int) result);
	}
}
