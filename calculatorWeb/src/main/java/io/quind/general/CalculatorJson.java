package io.quind.general;

public class CalculatorJson {
	private final int num1;
	private final int num2;
	private final int resultado;
	
	public CalculatorJson(int num1, int num2, int resultado) {
		this.num1 = num1;
		this.num2 = num2;
		this.resultado = resultado;
	}

	public int getNum1() {
		return num1;
	}

	public int getNum2() {
		return num2;
	}

	public int getResultado() {
		return resultado;
	}
	
}
